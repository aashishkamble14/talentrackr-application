package talentrackr;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

//This class for Entering Interview Feedback Notes
public class SendOffer 
{
	static Properties property;
	static FileInputStream fs;
	WebDriver driver;

	@Test(priority=1)
	public void InterviewYesNOFeedback() throws Exception
	{
		
		System.out.println("Interview Yes No Response");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		
		
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 30000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config1.properties");
		property= new Properties();
		property.load(fs);
		
		
		//driver.get("https://test.hurekadev.info/login"); //hardcoded correct code
		driver.get(property.getProperty("URL"));
				
		
		WebElement talenttrackrLogo = driver.findElement(By.xpath("//div/img[@class='logo']"));		
		wait.until(ExpectedConditions.visibilityOf(talenttrackrLogo));
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username"));
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password"));
		driver.findElement(By.className("btn-primary")).click();
		//Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Applicant Status']")));
		
		Thread.sleep(10000);
		driver.findElement(By.xpath("//h3[text()='Interview']")).click();
		//driver.get("http://dev.hurekadev.info/interviewed-list");
		
		Thread.sleep(10000);
		driver.findElement(By.xpath("//a[text()='test']")).click();   //Identified by text.
		//driver.get("http://dev.hurekadev.info/candidate-list/csr/18210/2812");
		
		Thread.sleep(10000);
		driver.findElement(By.xpath("//a[text()='Yes']")).click();				

		Thread.sleep(10000);
		driver.findElement(By.xpath("//textarea[@formcontrolname='notes']")).sendKeys(property.getProperty("InterviewfeedbackNotes"));				

		Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();				

		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//img[@class='logo']")).click();				

		Thread.sleep(5000);
		driver.findElement(By.xpath("//h3[text()='Offer']")).click();				

		Thread.sleep(10000);
		driver.findElement(By.xpath("//a[text()='test']")).click();  //Identified by text

		//Good to offer applicant logic
		Thread.sleep(5000);
		driver.findElement(By.xpath("//h6[text()='richard joy ']")).click();  // Identified by text - change here

		Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@class='btn with-bg-btn']")).click();

		Thread.sleep(5000);
		driver.findElement(By.xpath("//div[@class='ngx-editor-textarea']")).sendKeys(property.getProperty("OfferLetterNote"));				

		Thread.sleep(5000);
		driver.findElement(By.xpath("//button[@class='btn with-bg-btn']")).click();

		//To see Offered Applicants
		//Thread.sleep(5000);
		//driver.findElement(By.xpath("//a[@id='ngb-tab-11']")).click();
		
		System.out.println("offer sent successfully");
		
		Thread.sleep(2000);
		
		driver.quit();
	}
	
		
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
			System.out.println("2");
			driver.quit();
			//Utility.captureScreenshot(driver, result.getName());
			TakesScreenshot ts=(TakesScreenshot)driver;
			
			File source=ts.getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(source,new File("./ScreenShots/error.png"));
			
			System.out.println("Screenshot taken 2nd file");
			driver.quit();
			
		}
		//System.out.println("launching chrome browser 1st time");
	}
		
}
