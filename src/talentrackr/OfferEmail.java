package talentrackr;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

//This class is for Email Offer Acceptance
public class OfferEmail
{
	
	static Properties property;
	static FileInputStream fs;
	WebDriver driver;

	
	@Test(priority=1)
	public void MailOfferAccept() throws Exception
	{
		
		System.out.println("Offer Acceptance Method");
		//System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		//System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\geckodriver.exe");
		
		
		//Firefox logic
		//WebDriver driver = new FirefoxDriver();
		//driver.manage().window().maximize();
		
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Ashish\\Desktop\\Backup\\Selenium Softwares\\GeckoDriver-Firefox\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		
		/*
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 30000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		*/
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config1.properties");
		property= new Properties();
		property.load(fs);
	
		driver.get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		
		Thread.sleep(5000);
		//driver.findElement(By.id("identifierId")).sendKeys("guessusername4@gmail.com"); //Hardcoded Correct Code
		driver.findElement(By.id("identifierId")).sendKeys(property.getProperty("username4")); 
		
		

		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[text()='Next']")).click();
		Thread.sleep(5000);
		driver.findElement(By.name("password")).sendKeys(property.getProperty("password4"));  //change
		Thread.sleep(5000);
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[text()='Next']")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//b[text()='Offer Letter for Test from USA Paradisee']")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[text()='Yes']")).click();
		
		System.out.println("Offer accepted successfully");
				
			driver.quit();	
	}
	
	
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
			System.out.println("2");
			driver.quit();
			//Utility.captureScreenshot(driver, result.getName());
			TakesScreenshot ts=(TakesScreenshot)driver;
			
			File source=ts.getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(source,new File("./ScreenShots/error.png"));
			
			System.out.println("Screenshot taken 2nd file");
			driver.quit();
			
		}
		//System.out.println("launching chrome browser 1st time");
	}
		
}
