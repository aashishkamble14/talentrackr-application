package talentrackr;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.*;

import objectRepository.ObjectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;


//This class is for Interview Invitation Acceptance
public class InterviewEmail extends ApplicationMethods
{
	static Properties property;
	static FileInputStream fs;
	WebDriver driver;
	
	@Test(priority=1)
	public void InterviewEmailAccept() throws Exception
	{
		
		System.out.println("launching chrome browser 1st time");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		//driver.manage().window().maximize();
		
		//check();
		
		//Firefox logic
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Ashish\\Desktop\\Backup\\Selenium Softwares\\GeckoDriver-Firefox\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		
		/*
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 30000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		*/
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config1.properties");
		property= new Properties();
		property.load(fs);
		
		driver.get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
		
		Thread.sleep(5000);
		//driver.findElement(By.id("identifierId")).sendKeys("guessusername2@gmail.com"); //hardcoded correct code
		driver.findElement(By.id("identifierId")).sendKeys(property.getProperty("username3"));
		
		//change
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()='Next']")).click();
		Thread.sleep(2000);
		
		//driver.findElement(By.name("password")).sendKeys("passwordguess2");   //hardcoded correct code
		driver.findElement(By.name("password")).sendKeys(property.getProperty("password3"));
		
		Thread.sleep(2000);
		//driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div[2]/form/div[2]/div/div[3]/div[2]/div[1]/div/content/span")).click();
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//span[text()='Next']")).click();
		
		//driver.findElement(By.id("signIn")).click();
		//Thread.sleep(25000);
		System.out.println("Login done");
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Help Wanted Ad')]//ancestor::div[@class='offer']")));

		Thread.sleep(25000);
		driver.findElement(By.xpath("//b[text()='Test interview scheduled']")).click();   // Identified by text
		
		//driver.findElement(By.xpath("//span[text()='Test interview scheduled'][@id=':3e']")).click();
		
		//driver.findElement(By.xpath("//span[contains(text(),'Test interview scheduled']")).click();
		
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//a[text()='Yes']")).click();
		
		Thread.sleep(10000);
		
		System.out.println("Interview accepted successfully");
		
		driver.quit();		
}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
		
			driver.quit();
			//Utility.captureScreenshot(driver, result.getName());
			TakesScreenshot ts=(TakesScreenshot)driver;
			
			File source=ts.getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(source,new File("./ScreenShots/error.png"));
			
			System.out.println("Screenshot taken 2nd file");
			driver.quit();
			
		}
		//System.out.println("launching chrome browser 1st time");
	}
		
}

