package talentrackr;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;

public class ApplyAd
{
	static Properties property;
	static FileInputStream fs;
	
	//public static void main(String[] args) throws Exception 
	
	
	@Test(priority=1)
	public void Loginlogout() throws Exception
	{
		// TODO Auto-generated method stub

		System.out.println("launching chrome browser");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		//Thread.sleep(8000);
		
		//WebDriver driver = new ChromeDriver();
		//Thread.sleep(8000);
		//driver.manage().window().maximize();
		
		//ChromeOptions options = new ChromeOptions();
		//options.addArguments("disable-infobars");
		//WebDriver player = new ChromeDriver(options);
		
		//ChromeOptions options = null;
		//options.addArguments("disable-infobars"); 
		
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 30000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config1.properties");
		property= new Properties();
		property.load(fs);
		/*
		System.out.println(property.getProperty("username"));
		System.out.println(property.getProperty("password"));
		*/
		
		//driver.get("https://test.hurekadev.info/login");
		driver.get(property.getProperty("URL"));
		
		
		//Thread.sleep(5000);
		//driver.findElement(By.id("enterEmail")).sendKeys("guessusername20@gmail.com"); //Static correct code
		
		
		//Thread.sleep(1000);
		//driver.findElement(By.id("pwd")).sendKeys("Pass@123");  //Static correct code
		
		//Thread.sleep(2000);
		//driver.findElement(By.className("btn-primary")).click();   //Static correct code
		//Thread.sleep(3000);
		

		WebElement talenttrackrLogo = driver.findElement(By.xpath("//div/img[@class='logo']"));		
		wait.until(ExpectedConditions.visibilityOf(talenttrackrLogo));
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username"));
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password"));
		driver.findElement(By.className("btn-primary")).click();
		
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Applicant Status']")));
		
		Thread.sleep(10000);
		
		driver.get("https://test.hurekadev.info/all-hwa-list");
		
		Thread.sleep(1500);
		//span[text()='Sch']
		driver.findElement(By.xpath("//span[text()='Technical Assistant']")).click();   //Identified by text
		
		
		Thread.sleep(1000);
		//span[text()='Sch']
		driver.findElement(By.xpath("//small[text()='(View Published HWA)']")).click();
		
		
		String windowHandle = driver.getWindowHandle();
	    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
	    
	    Thread.sleep(2000);
	    ArrayList tabs = new ArrayList (driver.getWindowHandles());
	    System.out.println(tabs.size());
	    driver.switchTo().window((String) tabs.get(1)); 
		
		
		Thread.sleep(1200);
		//span[text()='Sch']
		driver.findElement(By.xpath("//a[@class='btn with-bg-btn']")).click();
		
		System.out.println("Switch Tab1");
		
		ArrayList tabs1 = new ArrayList (driver.getWindowHandles());
		System.out.println(tabs1.size());
		driver.switchTo().window((String) tabs1.get(2)); 
		System.out.println("Title is"+driver.getTitle());
		Thread.sleep(1800);
		
		
		System.out.println("Switch Tab2");
		
		
		driver.findElement(By.xpath("//input[@id='resumeFile']")).sendKeys("C:\\Users\\Ashish\\Desktop\\Resume.docx");
		//driver.findElement(By.xpath("//a[text()='Sign In']")).click();
		
		Thread.sleep(1000);
		System.out.println("Switch Tab3");
		 driver.findElement(By.className("btn")).click();
		
		 //Thread.sleep(5000);
		 
		 //driver.findElement(By.xpath("//input[@formcontrolname='firstName']")).sendKeys("Sam"); // Hardcoded correct code
		 driver.findElement(By.xpath("//input[@formcontrolname='firstName']")).sendKeys(property.getProperty("ApplicantFirstName"));
		 
		 
		 //Thread.sleep(5000);
		 //driver.findElement(By.xpath("//input[@formcontrolname='lastName']")).sendKeys("Sam");  // Hardcoded correct code
		 driver.findElement(By.xpath("//input[@formcontrolname='lastName']")).sendKeys(property.getProperty("ApplicantLastName"));
		 
		 
		 //Thread.sleep(5000);
		 //driver.findElement(By.xpath("//input[@formcontrolname='Email']")).sendKeys("Sam212211@gmail.com");  // Hardcoded correct code
		 driver.findElement(By.xpath("//input[@formcontrolname='Email']")).sendKeys(property.getProperty("ApplicantEmailId"));
		 
		 
		 //Thread.sleep(5000);
		 //driver.findElement(By.xpath("//input[@formcontrolname='cellPhone']")).sendKeys("1234567890"); // Hardcoded correct code
		 driver.findElement(By.xpath("//input[@formcontrolname='cellPhone']")).sendKeys(property.getProperty("ApplicantPhoneNo"));
		 
		 
		 //Thread.sleep(1000);
		 //driver.findElement(By.xpath("//input[@formcontrolname='zipCode']")).sendKeys("07006");  // Hardcoded correct code
		 driver.findElement(By.xpath("//input[@formcontrolname='zipCode']")).sendKeys(property.getProperty("ZipCode"));
		 
		 Thread.sleep(700);
		 driver.findElement(By.xpath("//input[@formcontrolname='city']")).click();
		
		 Thread.sleep(500);
		 //Thread.sleep(1000);
		 driver.findElement(By.xpath("//button[@class='btn mar-top']")).click();
		 
		 //Thread.sleep(3000);
		 
		 System.out.println("Current url is "+driver.getCurrentUrl());
		 Thread.sleep(5000);
		// driver.findElement(By.xpath("//label[@for='no0']")).click();
		 driver.findElement(By.xpath("//label[@for='yes0']")).click();
		 
		 //Thread.sleep(1000);
		 driver.findElement(By.xpath("//label[@for='yes1']")).click();
		 
		 //Thread.sleep(2000);
		 driver.findElement(By.xpath("//button[@class='btn mar-top']")).click();
		 Thread.sleep(5000);
		 driver.switchTo().window((String) tabs.get(0));
		 
		 Thread.sleep(2000);
		driver.get("https://test.hurekadev.info/all-hwa-list");
		 
		Thread.sleep(1000); 
		//Thread.sleep(8000);
		//verifyelementdashboard();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("navbar-toggler-icon")));
		 driver.findElement(By.className("navbar-toggler-icon")).click();
		 
		// Thread.sleep(8000);
			
		 //driver.findElement(By.className("nav-link")).click();
		 
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();
		 
		// Thread.sleep(3000);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pwd")));
		 
		 driver.quit();
		 
		 System.out.print("FRS login and logout done successfully.");
		 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Test
	public void verifyelementdashboard() throws Exception
	{
		Thread.sleep(2000);
	}
}
