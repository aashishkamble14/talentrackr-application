package talentrackr;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.*;

import objectRepository.ObjectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

//This class is for scheduling interview
public class ScheduleInterview extends ApplicationMethods
{
	static Properties property;
	static FileInputStream fs;
	WebDriver driver;
	
	@Test(priority=1)
	public void ScheduleInterview() throws Exception
	{
		
		System.out.println("launching chrome browser 1st time");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 30000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config1.properties");
		property= new Properties();
		property.load(fs);
		System.out.println(property.getProperty("URL"));
		System.out.println(property.getProperty("username"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("HWAName"));
		System.out.println(property.getProperty("Howmanypeopleyouneed"));
		System.out.println(property.getProperty("SkillsExperience"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("PaypalUsername"));
		System.out.println(property.getProperty("PaypalPassword"));
		
		//driver.get("https://test.hurekadev.info/login"); //hardcoded correct code
		driver.get(property.getProperty("URL"));
		
		WebElement talenttrackrLogo = driver.findElement(By.xpath("//div/img[@class='logo']"));		
		wait.until(ExpectedConditions.visibilityOf(talenttrackrLogo));
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username"));
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password"));
		driver.findElement(By.className("btn-primary")).click();
		//Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='Applicant Status']")));
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Interview')]")));
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Help Wanted Ad')]//ancestor::div[@class='offer']")));

		Thread.sleep(3000);
		//driver.findElement(By.className("//h3[text()='Applicant Status']")).click(); 
		
		//https://test.hurekadev.info/active-ad
		

		driver.get("https://test.hurekadev.info/active-ad");
		Thread.sleep(3000);
		System.out.println(property.getProperty("Click HWA Name"));

		driver.findElement(By.xpath("//a[text()='test']")).click();
		
		//driver.findElement(By.xpath("//a[text()='test']")).click();
		
		//New & Viewed
		
		Thread.sleep(4000);
		driver.findElement(By.xpath("//a[text()='harry john ']")).click();  // Identified by text
		
		//Shortlisted Code

		Thread.sleep(4000);
		driver.findElement(By.xpath("//button[@class='btn with-bg-btn']")).click();
		
		//Schedule Interview logic
		
		Thread.sleep(4000);
		driver.findElement(By.xpath("//button[@class='btn with-bg-btn']")).click();


		Thread.sleep(4000);
		driver.findElement(By.xpath("//button[@class='btn with-bg-btn']")).click();

		Thread.sleep(5000);
		driver.findElement(By.xpath("//input[@formcontrolname='date']")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[text()='15']")).click();   // Change date code & Identified by text
		
		//Start Date
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@formcontrolname='start']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//span[text()='Set']")).click();
		Thread.sleep(5000);
		String a=driver.findElement(By.xpath("//input[@formcontrolname='start']")).getText();
		System.out.println("Start time is "+a);
		
		//mode of interview
		//Thread.sleep(2000);
		//driver.findElement(By.xpath("//select[@id='mode']")).click();
		
		Thread.sleep(2000);
		WebElement ele1 = driver.findElement(By.xpath("//select[@id='mode']"));
		
		Select hire=new Select(ele1);
		hire.selectByIndex(1);
		
		Thread.sleep(3000);
		//driver.findElement(By.xpath("//input[@formcontrolname='where']")).sendKeys("04254787875");       // Phone Number - hardcoded
		driver.findElement(By.xpath("//input[@formcontrolname='where']")).sendKeys(property.getProperty("InterviewMode_PhoneNo")); 
		
		
		Thread.sleep(4000);
		driver.findElement(By.xpath("//button[@class='btn btn-primary']")).click();   //Interview scheduled i.e. interview pending

		Thread.sleep(12000);
		driver.findElement(By.className("navbar-toggler-icon")).click();
		 
		Thread.sleep(3000);
		 
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		
		System.out.println("Interview scheduled successfully");
		driver.quit();
		
}
	
		
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
		
			driver.quit();
			//Utility.captureScreenshot(driver, result.getName());
			TakesScreenshot ts=(TakesScreenshot)driver;
			
			File source=ts.getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(source,new File("./ScreenShots/error.png"));
			
			System.out.println("Screenshot taken 2nd file");
			driver.quit();
			
		}
		//System.out.println("launching chrome browser 1st time");
	}
		
}

