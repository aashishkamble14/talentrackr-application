package talentrackr;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.*;

import objectRepository.ObjectRepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class CreateHWA extends ApplicationMethods
{
	static Properties property;
	static FileInputStream fs;
	WebDriver driver;
	
	@Test(priority=1)
	public void CreateHWA1() throws Exception
	{
		
		System.out.println("launching chrome browser 1st time");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		//driver.manage().window().maximize();
		
		check();
		
		
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 30000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config.properties");
		property= new Properties();
		property.load(fs);
		System.out.println(property.getProperty("username"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("HWAName"));
		System.out.println(property.getProperty("Howmanypeopleyouneed"));
		System.out.println(property.getProperty("SkillsExperience"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("PaypalUsername"));
		System.out.println(property.getProperty("PaypalPassword"));
		
		
		driver.get("https://test.hurekadev.info/login");
		
		WebElement talenttrackrLogo = driver.findElement(By.xpath("//div/img[@class='logo']"));		
		wait.until(ExpectedConditions.visibilityOf(talenttrackrLogo));
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username"));
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password"));
		driver.findElement(By.className("btn-primary")).click();
		//Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a/img[@class='logo'])[1]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Interview')]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Help Wanted Ad')]//ancestor::div[@class='offer']")));

		WebElement help = driver.findElement(By.xpath("//h3[contains(text(),'Help Wanted Ad')]//ancestor::div[@class='offer']"));
		verifyelement1();
		//jse.executeScript("arguments[0].click();", help);
		driver.get("https://test.hurekadev.info/all-hwa-list");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.className("btn-success")));
		driver.findElement(By.className("btn-success")).click();
		
		//driver.get("https://test.hurekadev.info/login");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//h5[contains(text(),'Create HWA')]")));
		
		driver.findElement(By.xpath("//input[@formcontrolname='title']")).sendKeys(property.getProperty("HWAName"));		
		WebElement ele = driver.findElement(By.id("field_how_many_people_do_you_nee"));		
		Select type = new Select(ele);
		//type.selectByIndex(2);	
		type.selectByVisibleText(property.getProperty("Howmanypeopleyouneed"));		
		WebElement ele1 = driver.findElement(By.id("exampleSelect1"));
		
		Select hire=new Select(ele1);
		//hire.selectByIndex(0);
		hire.selectByVisibleText(property.getProperty("TypeOfPosition"));
		
        WebElement ele2= driver.findElement(By.id("field_describe_the_skills_and_ex"));
		
		Select hire2=new Select(ele2);
		//hire2.selectByIndex(2);
		hire2.selectByValue(property.getProperty("SkillsExperience"));
		
		 driver.findElement(By.className("ngx-editor-textarea")).sendKeys(property.getProperty("Description"));
		 
		 driver.findElement(By.xpath("(//button[@class='btn success-btn'])[1]")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(text(),'HWA Preview')]")));
		 driver.findElement(By.xpath("(//button[text()='Post Ad'])[1]")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h5[contains(text(),'Post Help Wanted Ad')]")));
		 driver.findElement(By.xpath("//label[text()='HWA start date']/../input")).click();
		 driver.findElement(By.xpath("//span[@class=\"owl-dt-calendar-cell-content owl-dt-calendar-cell-today\"]")).click();
		 driver.findElement(By.xpath("//span[text()='Set']")).click();
		
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//h5[contains(text(),'Post Help Wanted Ad')]")));
		 WebElement Pay = driver.findElement(By.xpath("//button[contains(text(),'Pay & Post HWA')]"));
		 jse.executeScript("arguments[0].click();", Pay);
		
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h5[contains(text(),'Payment')]")));
		 driver.findElement(By.className("payment_img")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Purchase details')]")));
		 verifyelement2();
		 //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='logo']")));
		// wait.until(ExpectedConditions.visibilityOf((WebElement) By.xpath("//span[@class='logo']")));
		// Thread.sleep(2000);
		 
		 //driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 //jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//input[@type='submit']//parent::div")));
		// jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//span[@class='logo']")));
		
		// explicitwait();
		//driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
	
		// driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Pay with PayPal')]")));
		 driver.findElement(By.id("email")).sendKeys(property.getProperty("PaypalUsername"));
		 driver.findElement(By.id("btnNext")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
		 driver.findElement(By.id("password")).sendKeys(property.getProperty("PaypalPassword"));	 
		 driver.findElement(By.id("btnLogin")).click();
		 
		 //driver.findElement(By.id("btnLogin")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("confirmButtonTop")));
		 //driver.findElement(By.id("confirmButtonTop")).click();
		// jse.executeScript("arguments[0].click();", driver.findElement(By.id("confirmButtonTop")));
		 //Thread.sleep(1500);
		// explicitwait();
		 verifyelement3();
		 WebElement Paynow = driver.findElement(By.id("confirmButtonTop"));
		 jse.executeScript("arguments[0].click();", Paynow);
		 
		 
		// driver.findElement(By.id("//input[@class='btn full confirmButton continueButton']")).click();
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("merchantReturnBtn")));
		// driver.findElement(By.id("merchantReturnBtn")).click();
		 //Thread.sleep(2000);
		 verifyelement4();
		 
		 jse.executeScript("arguments[0].click();", driver.findElement(By.id("merchantReturnBtn")));
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("navbar-toggler-icon")));
		 verifyelement5();
		 //Thread.sleep(2000);
		 driver.findElement(By.className("navbar-toggler-icon")).click();

		 //driver.findElement(By.className("nav-link")).click();
		 
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();
		 driver.close();
		 
		 System.out.print("HWA has been created in the system successfully.");
		 
		// wait.until(ExpectedConditions.vi
		 
	}
	/*
	
	@Test(priority=2)
	public void CreateHWA2() throws Exception
	{
		
		System.out.println("launching chrome browser 1st time");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		//driver.manage().window().maximize();
		
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 30000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config.properties");
		property= new Properties();
		property.load(fs);
		System.out.println(property.getProperty("username"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("HWAName"));
		System.out.println(property.getProperty("Howmanypeopleyouneed"));
		System.out.println(property.getProperty("SkillsExperience"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("PaypalUsername"));
		System.out.println(property.getProperty("PaypalPassword"));
		
		
		driver.get("https://test.hurekadev.info/login");
		
		WebElement talenttrackrLogo = driver.findElement(By.xpath("//div/img[@class='logo']"));		
		wait.until(ExpectedConditions.visibilityOf(talenttrackrLogo));
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username2"));
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password2"));
		driver.findElement(By.className("btn-primary")).click();
		//Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a/img[@class='logo'])[1]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Interview')]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Help Wanted Ad')]//ancestor::div[@class='offer']")));

		WebElement help = driver.findElement(By.xpath("//h3[contains(text(),'Help Wanted Ad')]//ancestor::div[@class='offer']"));
		verifyelement1();
		//jse.executeScript("arguments[0].click();", help);
		driver.get("https://test.hurekadev.info/all-hwa-list");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.className("btn-success")));
		driver.findElement(By.className("btn-success")).click();
		
		//driver.get("https://test.hurekadev.info/login");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//h5[contains(text(),'Create HWA')]")));
		
		driver.findElement(By.xpath("//input[@formcontrolname='title']")).sendKeys(property.getProperty("HWAName2"));		
		WebElement ele = driver.findElement(By.id("field_how_many_people_do_you_nee"));		
		Select type = new Select(ele);
		//type.selectByIndex(2);	
		type.selectByVisibleText(property.getProperty("Howmanypeopleyouneed2"));		
		WebElement ele1 = driver.findElement(By.id("exampleSelect1"));
		
		Select hire=new Select(ele1);
		//hire.selectByIndex(0);
		hire.selectByVisibleText(property.getProperty("TypeOfPosition2"));
		
        WebElement ele2= driver.findElement(By.id("field_describe_the_skills_and_ex"));
		
		Select hire2=new Select(ele2);
		//hire2.selectByIndex(2);
		hire2.selectByValue(property.getProperty("SkillsExperience2"));
		
		 driver.findElement(By.className("ngx-editor-textarea")).sendKeys(property.getProperty("Description2"));
		 
		 driver.findElement(By.xpath("(//button[@class='btn success-btn'])[1]")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(text(),'HWA Preview')]")));
		 driver.findElement(By.xpath("(//button[text()='Post Ad'])[1]")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h5[contains(text(),'Post Help Wanted Ad')]")));
		 driver.findElement(By.xpath("//label[text()='HWA start date']/../input")).click();
		 driver.findElement(By.xpath("//span[@class=\"owl-dt-calendar-cell-content owl-dt-calendar-cell-today\"]")).click();
		 driver.findElement(By.xpath("//span[text()='Set']")).click();
		
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//h5[contains(text(),'Post Help Wanted Ad')]")));
		 WebElement Pay = driver.findElement(By.xpath("//button[contains(text(),'Pay & Post HWA')]"));
		 jse.executeScript("arguments[0].click();", Pay);
		
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h5[contains(text(),'Payment')]")));
		 driver.findElement(By.className("payment_img")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Purchase details')]")));
		 verifyelement2();
		 //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='logo']")));
		// wait.until(ExpectedConditions.visibilityOf((WebElement) By.xpath("//span[@class='logo']")));
		// Thread.sleep(2000);
		 
		 //driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 //jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//input[@type='submit']//parent::div")));
		// jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//span[@class='logo']")));
		
		// explicitwait();
		//driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
	
		// driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Pay with PayPal')]")));
		 driver.findElement(By.id("email")).sendKeys(property.getProperty("PaypalUsername2"));
		 driver.findElement(By.id("btnNext")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
		 driver.findElement(By.id("password")).sendKeys(property.getProperty("PaypalPassword2"));	 
		 driver.findElement(By.id("btnLogin")).click();
		 
		 //driver.findElement(By.id("btnLogin")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("confirmButtonTop")));
		 //driver.findElement(By.id("confirmButtonTop")).click();
		// jse.executeScript("arguments[0].click();", driver.findElement(By.id("confirmButtonTop")));
		 //Thread.sleep(1500);
		// explicitwait();
		 verifyelement3();
		 WebElement Paynow = driver.findElement(By.id("confirmButtonTop"));
		 jse.executeScript("arguments[0].click();", Paynow);
		 
		 
		// driver.findElement(By.id("//input[@class='btn full confirmButton continueButton']")).click();
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("merchantReturnBtn")));
		// driver.findElement(By.id("merchantReturnBtn")).click();
		 //Thread.sleep(2000);
		 verifyelement4();
		 
		 jse.executeScript("arguments[0].click();", driver.findElement(By.id("merchantReturnBtn")));
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("navbar-toggler-icon")));
		 verifyelement5();
		 //Thread.sleep(2000);
		 driver.findElement(By.className("navbar-toggler-icon")).click();

		 //driver.findElement(By.className("nav-link")).click();
		 
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();
		 driver.close();
		 
		 System.out.print("HWA has been created in the system successfully.");
		 
		// wait.until(ExpectedConditions.vi
		 
	}
	

	
	@Test(priority=3)
	public void CreateHWA3() throws Exception
	{
		
		System.out.println("launching chrome browser 1st time");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		//driver.manage().window().maximize();
		
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 30000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config.properties");
		property= new Properties();
		property.load(fs);
		System.out.println(property.getProperty("username"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("HWAName"));
		System.out.println(property.getProperty("Howmanypeopleyouneed"));
		System.out.println(property.getProperty("SkillsExperience"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("PaypalUsername"));
		System.out.println(property.getProperty("PaypalPassword"));
		
		
		driver.get("https://test.hurekadev.info/login");
		
		WebElement talenttrackrLogo = driver.findElement(By.xpath("//div/img[@class='logo']"));		
		wait.until(ExpectedConditions.visibilityOf(talenttrackrLogo));
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username3"));
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password3"));
		driver.findElement(By.className("btn-primary")).click();
		//Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a/img[@class='logo'])[1]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Interview')]")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Help Wanted Ad')]//ancestor::div[@class='offer']")));

		WebElement help = driver.findElement(By.xpath("//h3[contains(text(),'Help Wanted Ad')]//ancestor::div[@class='offer']"));
		verifyelement1();
		//jse.executeScript("arguments[0].click();", help);
		driver.get("https://test.hurekadev.info/all-hwa-list");
		
		wait.until(ExpectedConditions.elementToBeClickable(By.className("btn-success")));
		driver.findElement(By.className("btn-success")).click();
		
		//driver.get("https://test.hurekadev.info/login");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//h5[contains(text(),'Create HWA')]")));
		
		driver.findElement(By.xpath("//input[@formcontrolname='title']")).sendKeys(property.getProperty("HWAName3"));		
		WebElement ele = driver.findElement(By.id("field_how_many_people_do_you_nee"));		
		Select type = new Select(ele);
		//type.selectByIndex(2);	
		type.selectByVisibleText(property.getProperty("Howmanypeopleyouneed3"));		
		WebElement ele1 = driver.findElement(By.id("exampleSelect1"));
		
		Select hire=new Select(ele1);
		//hire.selectByIndex(0);
		hire.selectByVisibleText(property.getProperty("TypeOfPosition3"));
		
        WebElement ele2= driver.findElement(By.id("field_describe_the_skills_and_ex"));
		
		Select hire2=new Select(ele2);
		//hire2.selectByIndex(2);
		hire2.selectByValue(property.getProperty("SkillsExperience3"));
		
		 driver.findElement(By.className("ngx-editor-textarea")).sendKeys(property.getProperty("Description3"));
		 
		 driver.findElement(By.xpath("(//button[@class='btn success-btn'])[1]")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(text(),'HWA Preview')]")));
		 driver.findElement(By.xpath("(//button[text()='Post Ad'])[1]")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h5[contains(text(),'Post Help Wanted Ad')]")));
		 driver.findElement(By.xpath("//label[text()='HWA start date']/../input")).click();
		 driver.findElement(By.xpath("//span[@class=\"owl-dt-calendar-cell-content owl-dt-calendar-cell-today\"]")).click();
		 driver.findElement(By.xpath("//span[text()='Set']")).click();
		
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//h5[contains(text(),'Post Help Wanted Ad')]")));
		 WebElement Pay = driver.findElement(By.xpath("//button[contains(text(),'Pay & Post HWA')]"));
		 jse.executeScript("arguments[0].click();", Pay);
		
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h5[contains(text(),'Payment')]")));
		 driver.findElement(By.className("payment_img")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Purchase details')]")));
		 verifyelement2();
		 //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='logo']")));
		// wait.until(ExpectedConditions.visibilityOf((WebElement) By.xpath("//span[@class='logo']")));
		// Thread.sleep(2000);
		 
		 //driver.findElement(By.xpath("//input[@type='submit']")).click();
		driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 //jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//input[@type='submit']//parent::div")));
		// jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//span[@class='logo']")));
		
		// explicitwait();
		//driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
	
		// driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Pay with PayPal')]")));
		 driver.findElement(By.id("email")).sendKeys(property.getProperty("PaypalUsername3"));
		 driver.findElement(By.id("btnNext")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
		 driver.findElement(By.id("password")).sendKeys(property.getProperty("PaypalPassword3"));	 
		 driver.findElement(By.id("btnLogin")).click();
		 
		 //driver.findElement(By.id("btnLogin")).click();
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("confirmButtonTop")));
		 //driver.findElement(By.id("confirmButtonTop")).click();
		// jse.executeScript("arguments[0].click();", driver.findElement(By.id("confirmButtonTop")));
		 //Thread.sleep(1500);
		// explicitwait();
		 verifyelement3();
		 WebElement Paynow = driver.findElement(By.id("confirmButtonTop"));
		 jse.executeScript("arguments[0].click();", Paynow);
		 
		 
		// driver.findElement(By.id("//input[@class='btn full confirmButton continueButton']")).click();
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("merchantReturnBtn")));
		// driver.findElement(By.id("merchantReturnBtn")).click();
		 //Thread.sleep(2000);
		 verifyelement4();
		 
		 jse.executeScript("arguments[0].click();", driver.findElement(By.id("merchantReturnBtn")));
		 
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("navbar-toggler-icon")));
		 verifyelement5();
		 //Thread.sleep(2000);
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();
		 driver.findElement(By.className("navbar-toggler-icon")).click();

		 //driver.findElement(By.className("nav-link")).click();
		 
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();
		 driver.close();
		 
		 System.out.print("HWA has been created in the system successfully.");
		 
		// wait.until(ExpectedConditions.vi
		 
	}*/
	
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
			System.out.println("2");
			driver.quit();
			//Utility.captureScreenshot(driver, result.getName());
			TakesScreenshot ts=(TakesScreenshot)driver;
			
			File source=ts.getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(source,new File("./ScreenShots/error.png"));
			
			System.out.println("Screenshot taken 2nd file");
			driver.quit();
			
		}
		//System.out.println("launching chrome browser 1st time");
	}
	
	
	
	
	
	
	
	/*
	@Test(priority=2)
	public void CreateHWA2() throws Exception
	{
		System.out.println("launching chrome browser");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		//driver.manage().window().maximize();
		
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config.properties");
		property= new Properties();
		property.load(fs);
		System.out.println(property.getProperty("username2"));
		System.out.println(property.getProperty("password2"));
		System.out.println(property.getProperty("HWAName2"));
		System.out.println(property.getProperty("Howmanypeopleyouneed2"));
		System.out.println(property.getProperty("SkillsExperience2"));
		System.out.println(property.getProperty("password2"));
		System.out.println(property.getProperty("PaypalUsername2"));
		System.out.println(property.getProperty("PaypalPassword2"));
		
		driver.get("https://test.hurekadev.info/login");
		Thread.sleep(1000);
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username2"));
		
		Thread.sleep(1000);
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password2"));
		
		Thread.sleep(2000);
		driver.findElement(By.className("btn-primary")).click();
		
		Thread.sleep(6000);
		
		driver.get("https://test.hurekadev.info/all-hwa-list");
		
		Thread.sleep(5000);
		
		driver.findElement(By.className("btn-success")).click();
		
		//driver.get("https://test.hurekadev.info/login");
		Thread.sleep(7000);
		driver.findElement(By.xpath("//input[@formcontrolname='title']")).sendKeys(property.getProperty("HWAName2"));
		Thread.sleep(2000);
		
		//driver.findElement(By.xpath("//*[@id='field_how_many_people_do_you_nee']")).click();
		
		WebElement ele = driver.findElement(By.id("field_how_many_people_do_you_nee"));
		
		Select type = new Select(ele);
		//type.selectByIndex(2);
		type.selectByVisibleText(property.getProperty("Howmanypeopleyouneed2"));
		
		
		WebElement ele1 = driver.findElement(By.id("exampleSelect1"));
		
		Select hire=new Select(ele1);
		//hire.selectByIndex(0);
		hire.selectByVisibleText(property.getProperty("TypeOfPosition2"));
		
		Thread.sleep(5000);
		
        WebElement ele2= driver.findElement(By.id("field_describe_the_skills_and_ex"));
		
		Select hire2=new Select(ele2);
		//hire2.selectByIndex(2);
		hire2.selectByValue(property.getProperty("SkillsExperience2"));
		
		//ngx-editor-textarea
		
		 driver.findElement(By.className("ngx-editor-textarea")).sendKeys(property.getProperty("Description2"));
		 
		 driver.findElement(By.xpath("(//button[@class='btn success-btn'])[1]")).click();
		 
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("(//button[text()='Post Ad'])[1]")).click();
		 
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("//label[text()='HWA start date']/../input")).click();
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("//span[@class=\"owl-dt-calendar-cell-content owl-dt-calendar-cell-today\"]")).click();
		 driver.findElement(By.xpath("//span[text()='Set']")).click();
		
		 Thread.sleep(2000);
		
		 //driver.findElement(By.className("btn success-btn1")).click();
		 
		 driver.findElement(By.xpath("//button[@class='btn success-btn']")).click();
		 
		 Thread.sleep(2000);
		 
		 driver.findElement(By.className("payment_img")).click();
		 
		 Thread.sleep(10000);
		 
		 driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("email")).sendKeys(property.getProperty("PaypalUsername2"));
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("btnNext")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("password")).sendKeys(property.getProperty("PaypalPassword2"));
		 
		 Thread.sleep(10000);
		 
		 driver.findElement(By.id("btnLogin")).click();
		 
		 Thread.sleep(10000);
		 
		 //driver.findElement(By.id("btnLogin")).click();
		 
		 Thread.sleep(10000);
		 
		 driver.findElement(By.id("confirmButtonTop")).click();
		 
		// driver.findElement(By.id("//input[@class='btn full confirmButton continueButton']")).click();
		
		 Thread.sleep(10000);
		 
		 driver.findElement(By.id("merchantReturnBtn")).click();
		 
		 Thread.sleep(10000);
		
		 driver.findElement(By.className("navbar-toggler-icon")).click();
		 
		 Thread.sleep(5000);
			
		 //driver.findElement(By.className("nav-link")).click();
		 
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.close();
		 
		 System.out.print("HWA has been created in the system successfully.");   
	}
	
	
	@Test(priority=3)
	public void CreateHWA3() throws Exception
	{
		System.out.println("launching chrome browser");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		//WebDriver driver = new ChromeDriver();
		//driver.manage().window().maximize();
		
		ChromeOptions options = new ChromeOptions(); 
		options.addArguments("disable-infobars"); 
		WebDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config.properties");
		property= new Properties();
		property.load(fs);
		System.out.println(property.getProperty("username3"));
		System.out.println(property.getProperty("password3"));
		System.out.println(property.getProperty("HWAName3"));
		System.out.println(property.getProperty("Howmanypeopleyouneed3"));
		System.out.println(property.getProperty("SkillsExperience3"));
		System.out.println(property.getProperty("password3"));
		System.out.println(property.getProperty("PaypalUsername3"));
		System.out.println(property.getProperty("PaypalPassword3"));
		
		driver.get("https://test.hurekadev.info/login");
		Thread.sleep(1000);
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username3"));
		
		Thread.sleep(1000);
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password3"));
		
		Thread.sleep(2000);
		driver.findElement(By.className("btn-primary")).click();
		
		Thread.sleep(6000);
		
		driver.get("https://test.hurekadev.info/all-hwa-list");
		
		Thread.sleep(5000);
		
		driver.findElement(By.className("btn-success")).click();
		
		//driver.get("https://test.hurekadev.info/login");
		Thread.sleep(7000);
		driver.findElement(By.xpath("//input[@formcontrolname='title']")).sendKeys(property.getProperty("HWAName3"));
		Thread.sleep(2000);
		
		WebElement ele = driver.findElement(By.id("field_how_many_people_do_you_nee"));
		
		Select type = new Select(ele);
		//type.selectByIndex(2);
		type.selectByVisibleText(property.getProperty("Howmanypeopleyouneed3"));
		
		WebElement ele1 = driver.findElement(By.id("exampleSelect1"));
		
		Select hire=new Select(ele1);
		//hire.selectByIndex(0);
		hire.selectByVisibleText(property.getProperty("TypeOfPosition3"));
		
		Thread.sleep(5000);
		
        WebElement ele2= driver.findElement(By.id("field_describe_the_skills_and_ex"));
		
		Select hire2=new Select(ele2);
		//hire2.selectByIndex(2);
		hire2.selectByValue(property.getProperty("SkillsExperience3"));
		
		//ngx-editor-textarea
		
		 driver.findElement(By.className("ngx-editor-textarea")).sendKeys(property.getProperty("Description3"));
		 
		 driver.findElement(By.xpath("(//button[@class='btn success-btn'])[1]")).click();
		 
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("(//button[text()='Post Ad'])[1]")).click();
		 
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("//label[text()='HWA start date']/../input")).click();
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("//span[@class=\"owl-dt-calendar-cell-content owl-dt-calendar-cell-today\"]")).click();
		 driver.findElement(By.xpath("//span[text()='Set']")).click();
		
		 Thread.sleep(2000);
		
		 //driver.findElement(By.className("btn success-btn1")).click();
		 
		 driver.findElement(By.xpath("//button[@class='btn success-btn']")).click();
		 
		 Thread.sleep(2000);
		 
		 driver.findElement(By.className("payment_img")).click();
		 
		 Thread.sleep(10000);
		 
		 driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("email")).sendKeys(property.getProperty("PaypalUsername3"));
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("btnNext")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("password")).sendKeys(property.getProperty("PaypalPassword3"));
		 
		 Thread.sleep(10000);
		 
		 driver.findElement(By.id("btnLogin")).click();
		 
		 Thread.sleep(10000);
		 
		 Thread.sleep(10000);
		 
		 driver.findElement(By.id("confirmButtonTop")).click();
		
		 Thread.sleep(10000);
		 
		 driver.findElement(By.id("merchantReturnBtn")).click();
		 
		 Thread.sleep(10000);
		
		 driver.findElement(By.className("navbar-toggler-icon")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.close();
		 
		 System.out.print("HWA has been created in the system successfully.");
	}
	*/






































































	@Test
	public void verifyelement1() throws Exception
	{
		//Thread.sleep(490);
		Thread.sleep(1000);
	}
	
	@Test
	public void verifyelement2() throws Exception
	{
		//Thread.sleep(1100);
		Thread.sleep(1000);
	}
	
	@Test
	public void verifyelement3() throws Exception
	{
		Thread.sleep(2000);
	}
	
	@Test
	public void verifyelement4() throws Exception
	{
		Thread.sleep(2000);
	}
	
	@Test
	public void verifyelement5() throws Exception
	{
		Thread.sleep(2000);
	}
	







@Test
public void check()
{
	System.out.println("check executed");
}
}
