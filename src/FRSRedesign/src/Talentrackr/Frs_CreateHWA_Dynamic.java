package Talentrackr;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class Frs_CreateHWA_Dynamic
{
	static Properties property;
	static FileInputStream fs;
	
	public static void main(String[] args) throws Exception 
	{
		// TODO Auto-generated method stub

		System.out.println("launching chrome browser");
		System.setProperty("webdriver.chrome.driver","C:\\SeleniumDrivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		//File reading logic
		fs= new FileInputStream(System.getProperty("user.dir")+"\\config.properties");
		property= new Properties();
		property.load(fs);
		System.out.println(property.getProperty("username"));
		System.out.println(property.getProperty("password"));
		System.out.println(property.getProperty("HWAName"));
		System.out.println(property.getProperty("Howmanypeopleyouneed"));
		System.out.println(property.getProperty("TypeOfPosition"));
		System.out.println(property.getProperty("SkillsExperience"));
		System.out.println(property.getProperty("Description"));
		System.out.println(property.getProperty("PaypalUsername"));
		System.out.println(property.getProperty("PaypalPassword"));
		
		
		
		
		
		driver.get("https://test.hurekadev.info/login");
		Thread.sleep(1000);
		driver.findElement(By.id("enterEmail")).sendKeys(property.getProperty("username"));
		
		Thread.sleep(1000);
		driver.findElement(By.id("pwd")).sendKeys(property.getProperty("password"));
		
		Thread.sleep(2000);
		driver.findElement(By.className("btn-primary")).click();
		
		Thread.sleep(6000);
		
		//driver.get("https://test.hurekadev.info/hwa-basic-info");
		
		//driver.findElement(By.xpath("//*[@id=\"login\"]/div/div[1]/div/p")).click();
		
		driver.get("https://test.hurekadev.info/all-hwa-list");
		
		Thread.sleep(5000);
		
		driver.findElement(By.className("btn-success")).click();
		
		//driver.get("https://test.hurekadev.info/login");
		Thread.sleep(7000);
		driver.findElement(By.xpath("//input[@formcontrolname='title']")).sendKeys(property.getProperty("HWAName"));
		Thread.sleep(2000);
		
		//driver.findElement(By.xpath("/html/body/app-root/app-hwa-basic-info/section/div[2]/div/div[1]/div[2]/div/form/div/div[1]/input[2]")).sendKeys("CSR");
		
		//driver.findElement(By.xpath("myInputStyle[name='title']")).sendKeys("Pass@123");
		
		//driver.findElement(By.id("field_how_many_people_do_you_nee")).click();
		
		//driver.findElement(By.xpath("//*[@id='field_how_many_people_do_you_nee']")).click();
		
		//Number of people
		WebElement ele = driver.findElement(By.id("field_how_many_people_do_you_nee"));
				
		String a=property.getProperty("Howmanypeopleyouneed");
		
		Select type = new Select(ele);
		//type.selectByIndex(2);
		
		type.selectByVisibleText(property.getProperty("Howmanypeopleyouneed"));
		//type.selectByIndex(property.getProperty("Howmanypeopleyouneed"));
		
		
		
		//Type of hire
		WebElement ele1 = driver.findElement(By.id("exampleSelect1"));
		
		Select hire=new Select(ele1);
		//hire.selectByIndex(0);
		
		hire.selectByVisibleText(property.getProperty("TypeOfPosition"));
		
		Thread.sleep(5000);
		
		
		
		//Required work experience
        WebElement ele2= driver.findElement(By.id("field_describe_the_skills_and_ex"));
		
		Select hire2=new Select(ele2);
		hire2.selectByValue(property.getProperty("SkillsExperience"));
		//hire2.selectByIndex(2);
		
		//type.selectByVisibleText(property.getProperty("SkillsExperience"));
		
		
		//ngx-editor-textarea
		
		 driver.findElement(By.className("ngx-editor-textarea")).sendKeys(property.getProperty("Description"));
		 /*
		 Thread.sleep(3000);
		 driver.findElement(By.xpath("(//img[@class='acc-arrow float-right'])[2]")).click();
		 
		 Thread.sleep(3000);
		 
		 driver.findElement(By.xpath("(//img[@class='add-icon'])[2]")).click();
		 
		 Thread.sleep(3000);
		 
		driver.findElement(By.id("inputqus")).sendKeys("aaa");
		 
		 Thread.sleep(3000);
		 
		 
		 driver.findElement(By.xpath("(//button[text()='Add'])[1]")).click();
		 
		 Thread.sleep(3000);
		 
		 //2
		 
		
		 driver.findElement(By.xpath("(//img[@class='acc-arrow float-right'])[3]")).click();
		 Thread.sleep(3000);
	 	//driver.findElement(By.xpath("(//img[@class='add-icon'])[3]")).click();
		 
		 Thread.sleep(3000);
		 
		 driver.findElement(By.name("skill_exp_ques")).sendKeys("bbb");
		 
		 Thread.sleep(3000);
		 
		 driver.findElement(By.xpath("(//button[text()='Add'])[2]")).click();
		 
		 Thread.sleep(3000);
		*/
		 
		 driver.findElement(By.xpath("(//button[@class='btn success-btn'])[1]")).click();
		 
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("(//button[text()='Post Ad'])[1]")).click();
		 
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("//label[text()='HWA start date']/../input")).click();
		 Thread.sleep(5000);
		 driver.findElement(By.xpath("//span[@class=\"owl-dt-calendar-cell-content owl-dt-calendar-cell-today\"]")).click();
		 driver.findElement(By.xpath("//span[text()='Set']")).click();
		
		
		 Thread.sleep(2000);
		
		 //driver.findElement(By.className("btn success-btn1")).click();
		 
		 driver.findElement(By.xpath("//button[@class='btn success-btn']")).click();
		 
		 Thread.sleep(2000);
		 
		 driver.findElement(By.className("payment_img")).click();
		 
		 Thread.sleep(10000);
		 
		 driver.findElement(By.xpath("//input[@class='continue-btn btn']")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("email")).sendKeys(property.getProperty("PaypalUsername"));
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("btnNext")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.findElement(By.id("password")).sendKeys(property.getProperty("PaypalPassword"));
		 

		 Thread.sleep(4000);
		 
		 driver.findElement(By.id("btnLogin")).click();
		 
		 Thread.sleep(10000);
		 
		 //driver.findElement(By.id("btnLogin")).click();
		 
		 Thread.sleep(10000);
		 
		 driver.findElement(By.id("confirmButtonTop")).click();
		 
		// driver.findElement(By.id("//input[@class='btn full confirmButton continueButton']")).click();
		
		 Thread.sleep(10000);
		 
		 driver.findElement(By.id("merchantReturnBtn")).click();
		 
		// driver.findElement(By.xpath("//input[@id='merchantReturnBtn']")).click();
		 
		 //driver.findElement(By.className("btn btn-secondary full submit receipt ng-binding ng-scope")).click();
		 
		 Thread.sleep(10000);
		
		 driver.findElement(By.className("navbar-toggler-icon")).click();
		 
		 Thread.sleep(5000);
			
		 //driver.findElement(By.className("nav-link")).click();
		 
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();
		 
		 Thread.sleep(5000);
		 
		 driver.close();
		 
		 System.out.print("HWA has been created in the system successfully.");
		   
	}

}
